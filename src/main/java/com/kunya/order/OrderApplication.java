package com.kunya.order;

import com.kunya.order.controller.messagequeued.PhoneMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@ComponentScan("com.kunya")
@EnableEurekaClient
@EnableScheduling
@RestController
public class OrderApplication {
	@Autowired
	private PhoneMessageSender phoneMessageSender;

	public static void main(String[] args) {
		SpringApplication.run(OrderApplication.class, args);
	}

    @RequestMapping("/testMq")
	public void test(){
		phoneMessageSender.sendPhoneMessage();

	}
}
