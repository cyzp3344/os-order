package com.kunya.order.dao;

import com.kunya.order.po.Order;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper {
    int deleteByPrimaryKey(@Param("orderId")Long orderId,@Param("table_prefix") String table_prefix);

    int insert(@Param("record")Order record,@Param("table_prefix") String table_prefix);

    int insertSelective(@Param("record")Order record,@Param("table_prefix") String table_prefix);

    Order selectByPrimaryKey(@Param("orderId")Long orderId,@Param("table_prefix") String table_prefix);

    int updateByPrimaryKeySelective(@Param("record")Order record,@Param("table_prefix") String table_prefix);

    int updateByPrimaryKey(@Param("record")Order record,@Param("table_prefix") String table_prefix);
   /**
    * @author zp
    * @Description: 根据订单id更新订单状态
    * @param
    * @date 2018/7/16 19:50
    */
    int updateStatusByPrimaryKey(@Param("orderId")Long orderId,@Param("status")byte status,@Param("table_prefix") String table_prefix);
    /**
     * @author zp
     * @Description: 根据订单id查询订单状态
     * @param
     * @date 2018/7/16 19:49
     */
//    byte selectStatusByPrimaryKey(@Param("orderId")Long orderId,@Param("table_prefix") String table_prefix);
}