package com.kunya.order.service;

import com.kunya.common.common.ServerResponse;

/**
 * @author zp
 * @Description: 订单service接口
 * @date 2018/7/16 19:50
 */
public interface IOrderService {
    /**
     * @author zp
     * @Description: 根据订单id更新订单状态
     * @param
     * @date 2018/7/16 20:08
     */
    ServerResponse updateStatusById(Long orderId, Byte status, String tablePrefix);
    /**
     * @author zp
     * @Description: 根据订单id查询订单状态
     * @param
     * @date 2018/7/16 20:22
     */
    ServerResponse<Byte> selectStatusById(Long orderId,String tablePrefix);
}
