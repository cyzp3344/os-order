package com.kunya.order.service.impl;

//import com.codingapi.tx.annotation.ITxTransaction;
import com.kunya.common.common.ResponseCode;
import com.kunya.common.common.ServerResponse;
import com.kunya.order.dao.OrderMapper;
import com.kunya.order.po.Order;
import com.kunya.order.service.IOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zp
 * @Description: 订单service接口实现类
 * @date 2018/7/16 19:51
 */
@Service("iOrderService")
public class OrderServiceImpl implements IOrderService{
    @Autowired
    private OrderMapper orderMapper;

/**
 * @author zp
 * @Description: 根据订单id更新订单状态
 * @param
 * @date 2018/7/16 20:07
 */
@Transactional
public ServerResponse updateStatusById(Long orderId,Byte status,String tablePrefix) {
    if (null==orderId|| StringUtils.isEmpty(tablePrefix)||null==status){

        return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
    }
    Order order=new Order();
    order.setOrderStatus(status);
    order.setOrderId(orderId);
    int resultCount=orderMapper.updateByPrimaryKeySelective(order,tablePrefix);
    if (resultCount<=0){
        return ServerResponse.createByErrorMessage("更新状态失败");
    }
    return ServerResponse.createBySuccess();
}

/**
 * @author zp
 * @Description: 根据订单id查询订单状态
 * @param
 * @date 2018/7/16 20:22
 */
public ServerResponse selectStatusById(Long orderId,String tablePrefix){
    if (orderId==null|| StringUtils.isEmpty(tablePrefix)){

        return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
    }
    Order order=orderMapper.selectByPrimaryKey(orderId,tablePrefix);
    if(null==order){
        return ServerResponse.createByErrorMessage("查询订单状态失败");
    }
    Byte status=order.getOrderStatus();

     return ServerResponse.createBySuccess(status);
    }
}
