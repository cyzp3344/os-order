package com.kunya.order.common;

import org.apache.commons.beanutils.PropertyUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author zp
 * @Description:
 * @date 2018/7/12 14:09
 */
@Aspect
@Component
public class RedissionLockAspect {

    @Autowired
    private RedissionLockTemplate lockTemplate;

    @Pointcut("@annotation(com.kunya.order.common.MyRedissionLock)")
    public void RedissonLockAspect() {
    }


    @Around(value = "RedissonLockAspect()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {

        //切点所在的类
        Class targetClass = pjp.getTarget().getClass();
        //使用了注解的方法名
        String methodName = pjp.getSignature().getName();
        //切点方法所有参数类型
        Class[] parameterTypes = ((MethodSignature) pjp.getSignature()).getMethod().getParameterTypes();
        //切点切的方法
        Method method = targetClass.getMethod(methodName, parameterTypes);

        Object[] arguments = pjp.getArgs();

        final String lockName = getLockName(method, arguments);

        MyRedissionLock annotation = method.getAnnotation(MyRedissionLock.class);
        //调用“尝试锁”，默认不使用公平锁
        return tryLock(pjp, annotation, lockName, false);
    }


    //抛异常之后抛出运行时异常
    @AfterThrowing(value = "RedissonLockAspect()", throwing = "ex")
    public void afterThrowing(Throwable ex) {
        throw new RuntimeException(ex);
    }

    public String getLockName(Method method, Object[] args) {
        Objects.requireNonNull(method);
        MyRedissionLock annotation = method.getAnnotation(MyRedissionLock.class);

        String lockName = annotation.lockName(),
                param = annotation.param();

        if (isEmpty(lockName)) {
            if (args.length > 0) {
                if (isNotEmpty(param)) {
                    Object arg;
                    if (annotation.argNum() > 0) {
                        arg = args[annotation.argNum() - 1];
                    } else {
                        arg = args[0];
                    }
                    lockName = String.valueOf(getParam(arg, param));
                } else if (annotation.argNum() > 0) {
                    lockName = args[annotation.argNum() - 1].toString();
                }
            }
        }

        if (isNotEmpty(lockName)) {
            String preLockName = annotation.lockNamePre(),
                    postLockName = annotation.lockNamePost(),
                    separator = annotation.separator();

            StringBuilder lName = new StringBuilder();
            if (isNotEmpty(preLockName)) {
                lName.append(preLockName).append(separator);
            }
            lName.append(lockName);
            if (isNotEmpty(postLockName)) {
                lName.append(separator).append(postLockName);
            }

            lockName = lName.toString();

            return lockName;
        }

        throw new IllegalArgumentException("Can't get or generate lockName accurately!");
    }

    /**
     * 从方法参数获取数据
     *
     * @param param
     * @param arg   方法的参数数组
     * @return
     */
    public Object getParam(Object arg, String param) {
        if (isNotEmpty(param) && arg != null) {
            try {
                Object result = PropertyUtils.getProperty(arg, param);
                return result;
            } catch (NoSuchMethodException e) {
                throw new IllegalArgumentException(arg + "没有属性" + param + "或未实现get方法。", e);
            } catch (Exception e) {
                throw new RuntimeException("", e);
            }
        }
        return null;
    }

    /**
     * @param
     * @author zp
     * @Description: 判断是否使用尝试锁，是否使用公平锁
     * @date 2018/5/9 13:54
     */
/*    public Object lock(ProceedingJoinPoint pjp, Method method, final String lockName) {
        // 获取注解对象
        RedissonLock annotation = method.getAnnotation(RedissonLock.class);
        //是否是公平锁
        boolean fairLock = annotation.fairLock();
        //是否是尝试锁
        boolean tryLock = annotation.tryLock();
        //如果是尝试锁，调用trylock()
        if (tryLock) {
            return tryLock(pjp, annotation, lockName, fairLock);
        } else {
            //如果不是尝试锁调用 lock(pjp,lockName, fairLock) 方法重载
            return lock(pjp, lockName, fairLock);
        }
    }*/
    /**
     * @author zp
     * @Description: 不使用尝试锁
     * @param
     * @date 2018/5/9 13:52
     */
/*    public Object lock(ProceedingJoinPoint pjp, final String lockName, boolean fairLock) {
        return lockTemplate.lock(new RedissionLockCallback<Object>() {
            @Override
            public Object process() {
                return proceed(pjp);
            }

            @Override
            public String getLockName() {
                return lockName;
            }
        }, fairLock);
    }*/

    /**
     * @params  pjp ：切点对象 annotation：注解对象 lockName ： 锁名称 fairLock ：是否使用公平锁
     * @author zp
     * @Description: 使用尝试锁
     * @date 2018/5/9 13:53
     */
    public Object tryLock(ProceedingJoinPoint pjp, MyRedissionLock annotation, final String lockName, boolean fairLock) {

        long waitTime = annotation.waitTime(),
                leaseTime = annotation.leaseTime();
        TimeUnit timeUnit = annotation.timeUnit();

        return lockTemplate.tryLock(new RedissionLockCallback<Object>() {
            @Override
            public Object process() {
                return proceed(pjp);
            }

            @Override
            public String getLockName() {
                return lockName;
            }
        }, waitTime, leaseTime, timeUnit, fairLock);
    }

    public Object proceed(ProceedingJoinPoint pjp) {
        try {
            return pjp.proceed();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    private boolean isEmpty(Object str) {
        return str == null || "".equals(str);
    }

    private boolean isNotEmpty(Object str) {
        return !isEmpty(str);
    }


}
