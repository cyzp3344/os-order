package com.kunya.order.common;

import com.kunya.common.util.PropertiesUtil;
import org.redisson.Redisson;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author zp
 * @Description:
 * @date 2018/7/12 12:00
 */
@Component
public class RedissionManager {
    private  static  final Logger logger= LoggerFactory.getLogger(RedissionManager.class);

    private Config config = new Config();

    private Redisson redisson = null;

    public Redisson getRedisson() {
        return redisson;
    }

    private static String redis1Ip = PropertiesUtil.getProperty("redis1.ip");
    private static Integer redis1Port = Integer.parseInt(PropertiesUtil.getProperty("redis1.port"));

    @PostConstruct
    private void init(){
        try {
            config.useSingleServer().setAddress(new StringBuilder().append(redis1Ip).append(":").append(redis1Port).toString());

            redisson = (Redisson) Redisson.create(config);

            logger.info("初始化Redisson结束");
        } catch (Exception e) {
            logger.error("redisson init error",e);
        }
    }



}