package com.kunya.order.common;

/**
 * @author zp
 * @Description:
 * @date 2018/7/12 14:13
 */
public interface RedissionLockCallback<T> {
    //在此放入业务逻辑
    public T process();
    //获取分布式锁名称
    public String getLockName();

}