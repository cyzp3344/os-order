package com.kunya.order.task;

import com.kunya.order.common.MyRedissionLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author zp
 * @Description:
 * @date 2018/7/12 14:34
 */
@Slf4j
@Component
public class CloseOrderTask {
   //分布式定时任务逻辑
    // 1.每一分钟检查一次
    //2.分布式部署下，应只有一个实例获取分布式锁
    //3.获取到分布式锁的实例线程执行关单业务逻辑，其他未获取到锁的程序实例等待下一分钟尝试获取
     //定时关单业务逻辑
    //1.检查订单生成时间
    //2.与当前时间求差
    //3.判断是否大于24小时
    //4.大于24小时执行update订单 操作
    //5.执行成功进行update库存操作
//    @Scheduled(cron="0 */1 * * * ?")
    @MyRedissionLock (waitTime = 0, leaseTime = 50, lockName = "zz")
    public void closeOrderTask(){
        log.info("定时任务启动",Thread.currentThread().getName());
    }
}
