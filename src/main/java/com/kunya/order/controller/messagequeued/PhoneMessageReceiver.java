package com.kunya.order.controller.messagequeued;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author zp
 * @Description: 调用短信接口消息队列(监听hello队列) 消费者
 * @date 2018/7/16 13:52
 */
@Component
@RabbitListener(queues = "hello")
public class PhoneMessageReceiver {
    /**
     * @author zp
     * @Description: 对消息处理的指定方法。根据消息中的参数，调用发送短信接口
     * @param
     * @date 2018/7/16 13:56
     */
    @RabbitHandler
    public void process(String  context) {

        //todo 调用发送短信接口
        System.out.println("Receiver : " + context);
    }

}
