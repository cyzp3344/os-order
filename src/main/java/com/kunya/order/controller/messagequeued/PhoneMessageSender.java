package com.kunya.order.controller.messagequeued;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zp
 * @Description: 调用短信接口消息队列 生产者
 * @date 2018/7/16 13:46
 */
@Component
public class PhoneMessageSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    /**
     * @author zp
     * @Description: 向队列发送消息 ，参数 手机号，和消息模板id
     * @param
     * @date 2018/7/16 13:54
     */
    public void sendPhoneMessage(){
        //todo 传递短信接口所需参数
        String context="hello test";
        this.rabbitTemplate.convertAndSend("hello",context);

    }
}
