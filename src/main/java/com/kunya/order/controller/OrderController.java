package com.kunya.order.controller;

import com.kunya.common.common.ServerResponse;
import com.kunya.common.util.CommonUtil;
import com.kunya.order.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zp
 * @Description:
 * @date 2018/7/16 13:48
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private IOrderService iOrderService;

    /**
     * @author zp
     * @Description: 根据订单id修改订单状态
     * @param
     * @date 2018/7/16 20:11
     */
    @RequestMapping("/updateOrderStatus")
    public ServerResponse updateOrderStatus(Long orderId, byte status, String tablePrefix){

        return iOrderService.updateStatusById(orderId,status, tablePrefix);
    }
    /**
     * @author zp
     * @Description: 根据订单id查询订单状态
     * @param
     * @date 2018/7/16 20:12
     */
    @RequestMapping("/searchOrderStatus")
    public ServerResponse searchOrderStatus(Long orderId,String tablePrefix){

        return  iOrderService.selectStatusById(orderId,tablePrefix);
    }
}
